(function() {
  var ε = 1e-6, ε2 = ε * ε, π = Math.PI, halfπ = π / 2, sqrtπ = Math.sqrt(π), radians = π / 180, degrees = 180 / π;
  function sinci(x) {
    return x ? x / Math.sin(x) : 1;
  }
  function sgn(x) {
    return x > 0 ? 1 : x < 0 ? -1 : 0;
  }
  function asin(x) {
    return x > 1 ? halfπ : x < -1 ? -halfπ : Math.asin(x);
  }
  function acos(x) {
    return x > 1 ? 0 : x < -1 ? π : Math.acos(x);
  }
  function asqrt(x) {
    return x > 0 ? Math.sqrt(x) : 0;
  }
  var projection = d3.geo.projection, projectionMutator = d3.geo.projectionMutator;
  function parallel1Projection(projectAt) {
    var φ0 = 0, m = projectionMutator(projectAt), p = m(φ0);
    p.parallel = function(_) {
      if (!arguments.length) return φ0 / π * 180;
      return m(φ0 = _ * π / 180);
    };
    return p;
  }
  function cylindricalStereographic(φ0) {
    var cosφ0 = Math.cos(φ0);
    function forward(λ, φ) {
      return [ λ * cosφ0, (1 + cosφ0) * Math.tan(φ * .5) ];
    }
    forward.invert = function(x, y) {
      return [ x / cosφ0, Math.atan(y / (1 + cosφ0)) * 2 ];
    };
    return forward;
  }
  (d3.geo.cylindricalStereographic = function() {
    return parallel1Projection(cylindricalStereographic);
  }).raw = cylindricalStereographic;
})();
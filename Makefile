GENERATED_FILES = \
	dist/d3.geo.projection.js \
	dist/d3.geo.projection.min.js \
	dist/d3.geo.projection.cylindrical-stereographic.js \
	dist/d3.geo.projection.cylindrical-stereographic.min.js


all: $(GENERATED_FILES)

.PHONY: clean all test

dist/d3.geo.projection.js: $(shell node_modules/.bin/smash --list src/index.js)
	@rm -f $@
	node_modules/.bin/smash src/index.js | node_modules/.bin/uglifyjs - -b indent-level=2 -o $@
	@chmod a-w $@

dist/d3.geo.projection.min.js: dist/d3.geo.projection.js
	@rm -f $@
	node_modules/.bin/uglifyjs $< -c -m -o $@

dist/d3.geo.projection.cylindrical-stereographic.js: $(shell node_modules/.bin/smash --list src/index-cylindrical-stereographic.js)
	@rm -f $@
	node_modules/.bin/smash src/index-cylindrical-stereographic.js | node_modules/.bin/uglifyjs - -b indent-level=2 -o $@
	@chmod a-w $@

dist/d3.geo.projection.cylindrical-stereographic.min.js: dist/d3.geo.projection.cylindrical-stereographic.js
	@rm -f $@
	node_modules/.bin/uglifyjs $< -c -m -o $@

test:
	@node_modules/.bin/vows

clean:
	rm -f -- $(GENERATED_FILES)